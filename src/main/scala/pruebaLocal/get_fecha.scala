package LucaLocal

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

object get_fecha {
  val logg = Logger.getLogger("org")
  logg.setLevel(Level.WARN)

  def main(args : Array[String]) {

    implicit val spark = SparkSession
      .builder().master("local[1]")
      .appName("PruebasLocal")
      .getOrCreate()

    import spark.implicits._

    val dataDate = "08/08/2022"
    val dataDateColumn = to_date(lit(dataDate), "dd/MM/yyyy")
    val entidad = "RECIBOS"
    val rutaparam = "tablas_param/"

    println("tabla_soporte")
    getParametricTable("tabla_soporte",dataDateColumn).show(5,false)

    println("modelo_f1")
    getParametricTable("modelo_f1_mod_vig",dataDateColumn).show(5,false)

    println("param_tr")
    getParametricTable2(rutaparam + "param_tr_cod_cob_int",dataDateColumn).show(5,false)

    //println("param_tr")
    //getParametricTable("param_tr_cod_cob_int",dataDateColumn).show(5,false)
  } //FIN MAIN
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def getParametricTable(tableName: String, data_date:Column)(implicit spark: SparkSession): DataFrame ={
    //import spark.implicits._
    val rutaaux = "src/main/Datos/"
    println("la ruta completa es:")
    val rutacom = rutaaux+tableName+".csv"
    println(rutacom)
    spark.read.option("delimiter", ";").option("header", "true").csv(rutacom)
      .withColumn("desde",to_date(col("desde"),"dd/MM/yyyy"))
      .withColumn("hasta",to_date(col("hasta"),"dd/MM/yyyy"))
      .filter(data_date.between(col("desde"),col("hasta")))
      .drop("desde","hasta")
  }
  def getParametricTable2(tableName: String, data_date:Column)(implicit spark: SparkSession): DataFrame ={
    //import spark.implicits._
    val rutaaux = "src/main/Datos/"
    println("la ruta completa es:")
    val rutacom = rutaaux+tableName+".csv"
    println(rutacom)
    val df_1 = spark.read.option("delimiter", ";").option("header", "true").csv(rutacom)
    df_1.show(5,false)
    val df_2 = df_1
      .withColumn("desde",to_date(col("desde"),"dd/MM/yyyy"))
      .withColumn("hasta",to_date(col("hasta"),"dd/MM/yyyy"))
    df_2.show(5,false)
    val df_3 = df_2
      .filter(data_date.between(col("desde"),col("hasta")))
    df_3.show(5,false)
    val df_4 = df_3
      .drop("desde","hasta")
    df_4
  }
}


