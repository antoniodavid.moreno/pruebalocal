package LucaLocal

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

/**
 * Unai Iparraguirre
 * David Moreno
 */

object PruebasLocal {
  val logg = Logger.getLogger("org")
  logg.setLevel(Level.WARN)

  def main(args: Array[String]) {

    implicit val spark = SparkSession
      .builder().master("local[1]")
      .appName("Prueba_Local")
      .getOrCreate()

    import spark.implicits._

    /** CARGAMOS MUESTRA * */
    val muestra = spark.read.option("delimiter", ";").option("header", "true").csv("src/main/Datos/Muestra.csv")
      .filter(col("estado_f0") =!= "ko")
    /** tablas Logs * */
    val log_validaciones = spark.read.option("delimiter", ";").option("header", "true").csv("src/main/Datos/log_validaciones.csv")
    val log_validaciones_detalle = spark.read.option("delimiter", ";").option("header", "true").csv("src/main/Datos/log_validaciones_detalle.csv")

    val dataDate = "08/08/2022" // Futuras variables
    val dataDateColumn = to_date(lit(dataDate), "dd/MM/yyyy")
    val entidad = "RECIBOS" // Futura variable

    val df_tec = getF1Tec(muestra, dataDateColumn, entidad)
    //resu.write.format("csv").mode("overwrite").option("header","true").save("src/main/Datos/Output/prueba.csv")
    df_tec.show(numRows = 10, truncate = false)

    val valtec = valGeneral(df_tec,entidad,"F1 TECNICA","estado_f1_tec",log_validaciones)
    valtec.show(10, false)
    val valtecdet = valDetalle(df_tec,entidad,"F1 TECNICA","estado_f1_tec","seudoEstado",log_validaciones_detalle)
    valtecdet.show(100, false)

    val df_func  = getF1Func(df_tec.drop("seudoEstado").filter(col("estado_f1_tec") =!= "ko"),entidad,dataDateColumn)
    df_func.show(10, false)

    val valfun = valGeneral(df_func, entidad,"F1 FUNCIONAL","estado_f1_func",valtec)
    valfun.show(10, false)

    val valfundet = valDetalleFunc(df_func,entidad,"F1 FUNCIONAL","estado_f1_func","seudoEstado",valtecdet)
    valfundet.show(100,false)

    val tablaf1 = getEstado(df_func.drop("seudoEstado"),"estado_f1_tec","estado_f1_func","estado_f1")
    tablaf1.show(5,false)

  } //FIN MAIN

  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%% DEFINICIÓN FUNCIONES AUXILIARES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  //
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def crossWithParametricTable(muestra: DataFrame, parametricDF: DataFrame, inputColumns: Seq[String])(implicit spark:SparkSession): DataFrame = {
    muestra.join(broadcast(parametricDF), inputColumns, "left")
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def getParametricTable(tableName: String, data_date: Column)(implicit spark:SparkSession): DataFrame = {
    val rutaaux = "src/main/Datos/"
    spark.read.option("delimiter", ";").option("header", "true").csv(rutaaux + tableName + ".csv")
      .withColumn("desde", to_date(col("desde"), "dd/MM/yyyy")) //realizamos antes el cambio a fecha de hasta y desde
      .withColumn("hasta", to_date(col("hasta"), "dd/MM/yyyy"))
      .filter(data_date.between(col("desde"), col("hasta")))
      .drop("desde", "hasta")
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def getParametricTable2(tableName: String, data_date: Column)(implicit spark: SparkSession): DataFrame = {
    val rutaaux = "src/main/Datos/"
    val rutacom = rutaaux + tableName + ".csv"
    spark.read.option("delimiter", ";").option("header", "true").csv(rutacom)
      .withColumn("desde", to_date(col("desde"), "MM/dd/yyyy"))
      .withColumn("hasta", to_date(col("hasta"), "MM/dd/yyyy"))
      .filter(data_date.between(col("desde"), col("hasta")))
      .drop("desde", "hasta")
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def getEnrichment(modelo_f1: DataFrame,dataDateColumn:Column)(implicit spark:SparkSession): DataFrame = {
    val a = modelo_f1
      .select("nombre_campo", "tabla_soporte")
      .groupBy("tabla_soporte").agg(collect_list("nombre_campo").as("variable_salida"))
    val b = getParametricTable("tabla_soporte", dataDateColumn)
      .filter(col("direccion")==="ENTRADA")
      .select("nombre_tabla","nombre_campo")
    val c = getParametricTable("modelo_f0_PL",dataDateColumn)
    a.join(b,a("tabla_soporte")===b("nombre_tabla"),"left")
      .select("tabla_soporte","variable_salida","nombre_campo")
      .join(c,Seq("nombre_campo"),"left")
      .select("tabla_soporte","variable_salida","nombre_campo","nullabilidad_campo")
      .dropDuplicates()
      .groupBy("tabla_soporte","variable_salida")
      .agg(collect_list("nombre_campo").as("variable_entrada"),
        collect_list("nullabilidad_campo").as("nulla"))
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def getF1Tec(df0: DataFrame, dataDateColumn: Column, entidad: String)(implicit spark: SparkSession): DataFrame = {
    // Cargamos modelo_f1 y filtramos por la entidad que queremos
    val modelo_f1 = getParametricTable("modelo_f1_mod_vig", dataDateColumn)
      .filter(col("entidad") === entidad.toUpperCase())

    val datosTablas = getEnrichment(modelo_f1,dataDateColumn)
      .collect().map(x => (x.getString(0), x.getSeq[String](1),x.getSeq[String](2),x.getSeq[String](3)))
    //println(datosTablas.toList)
    // Nombres de las tablas parametricas en Seq

    // Creamos col seudoEstado con un inicio establecido
    val df1 = df0.withColumn("seudoEstado", lit(""))

    val dfprueba = datosTablas.foldLeft(df1) {
      (df, x) => {
        val cros = crossWithParametricTable(df, getParametricTable2("tablas_param/" + x._1.toLowerCase(), dataDateColumn), x._3)
        setStatus2(cros,x._2,x._4)
      }
    }
    setStatusCol(dfprueba, "estado_f1_tec", "seudoEstado")
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def getF1Func(df: DataFrame,entidad:String,dataDate: Column)(implicit sparkSession: SparkSession): DataFrame = {
    val df1 = df.withColumn("seudoEstado", lit(""))

    val valfunf1 = getParametricTable("val_func_f1", dataDate)
      .filter(col("entidad") === "recibos_test")
      .filter(col("activa") === 't')
      .select("validacion", "bloqueante", "cod_error","desc_error")
      .collect().map(x => (x.getString(0), x.getString(1), x.getString(2),x.getString(3)))

    println(valfunf1.toList)

    val df2 = valfunf1.foldLeft((df1,0)) {
      (df, i) => {
        val x = df._1.withColumn("seudoEstado",
          when(expr(i._1), col("seudoEstado"))
            .when(lit('t').equalTo(i._2), concat(col("seudoEstado"), lit("\tko-" + i._3 + i._4)))
            .otherwise(concat(col("seudoEstado"), lit("\twarn-" + i._3 + i._4))))
        if (df._2 < 5) (x, df._2 + 1)
        else {
          x.cache()
          x.count()
          (x, 0)
        }
      }
    }

    var df3 = df1
    for (a<-valfunf1){
      df3 = df3.withColumn("seudoEstado",
        when(expr(a._1), col("seudoEstado"))
          .when(lit('t').equalTo(a._2), concat(col("seudoEstado"), lit("\tko-" + a._3 + a._4)))
          .otherwise(concat(col("seudoEstado"), lit("\twarn-" + a._3 + a._4))))
    }
    println("el del bucle")
    df3.show(5,false)

    setStatusCol(df2._1, "estado_f1_func", "seudoEstado")
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def getEstado(df:DataFrame,estTec:String,estFun:String,estFinal:String)(implicit sparkSession: SparkSession): DataFrame ={
    df.withColumn(estFinal,when(col(estTec)===lit("warn")&&col(estFun)===lit("ok"),lit("warn")).otherwise(col(estFun)))
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def setStatusCol(df: DataFrame, nombreCol: String, compCol: String)(implicit sparkSession: SparkSession): DataFrame = {
    df.withColumn(nombreCol, when(col(compCol).contains("ko-"), "ko").when(col(compCol).contains("warn-"), "warn")
      .otherwise("ok"))
  }
  // %%%%%%%%%%5
  def setStatus2(df1: DataFrame,outNames: Seq[String],entryNulls:Seq[String])(implicit spark: SparkSession): DataFrame = {
      outNames.foldLeft(df1) {
        (df, i) => {
          val d1 = df.withColumn("seudoEstado",
            when(col(i).isNull && entryNulls.find( _ == "f" ).isEmpty, concat(col("seudoEstado"), lit("\tko-null-" + i)))
              .when(col(i).isNull && !(entryNulls.find( _ == "f" ).isEmpty),concat(col("seudoEstado"), lit("\twarn-null-" + i)))
              .when(col(i) === "" && entryNulls.find( _ == "f" ).isEmpty, concat(col("seudoEstado"), lit("\tko-empty-" + i)))
              .when(col(i) === "" && !(entryNulls.find( _ == "f" ).isEmpty), concat(col("seudoEstado"), lit("\twarn-null-" + i)))
              .otherwise(col("seudoEstado")))
          d1.show(10,false)
          d1
        }
      }
    }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def valGeneral(df: DataFrame, entidad: String,nombreProc:String,colEstado:String, valG: DataFrame)(implicit sparkSession: SparkSession): DataFrame = {
    val a = df.select(col(colEstado),col("cod_sociedad").as("sociedad"),col("sist_origen"))
      .withColumn("nombre_proceso", lit(nombreProc))
      .withColumn("entidad", lit(entidad))
      .withColumn("ID_proceso", concat(col("nombre_proceso"),lit(" "), col("entidad")))
      .withColumn("fecha_timestamp", current_timestamp())
      .groupBy(col("sociedad"), col("sist_origen"), col("entidad"), col("fecha_timestamp"), col("id_proceso"), col("nombre_proceso"))
      .agg(count(when(col(colEstado) === "ok", 1)).as("numero_ok"), count(when(col(colEstado) === "warn", 1)).as("numero_warn"), count(when(col(colEstado) === "ko", 1)).as("numero_ko"))
      .withColumn("descripcion", when(col("numero_ko") === 0, lit("Proceso con éxito")).otherwise(lit("Error bloqueante en la validacion "+nombreProc)))
      .withColumn("estado_proceso", when(col("numero_ko") =!= 0, lit("ko")).when(col("numero_warn") =!= 0, lit("warn")).otherwise(lit("ok")))
    valG.unionByName(a)
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def valDetalle(validacion_df: DataFrame, entidad: String, nombreProc:String, colEstado:String, colAux:String, valD: DataFrame)(implicit sparkSession: SparkSession): DataFrame = {
    val a = validacion_df.filter(col(colEstado) =!= "ok").select(explode(split(col(colAux), "\t")).as("fails"))
      .withColumn("nombre_proceso", lit(nombreProc))
      .withColumn("entidad", lit(entidad))
      .withColumn("codigo_error", cod_error(col("fails")))
      .withColumn("id_proceso", concat(col("nombre_proceso"), col("entidad")))
      .withColumn("descripcion_error", concat(desc_error(col("codigo_error")), split(col("fails"), "-")(2)))
      .withColumn("tipo_error", when(col("codigo_error").contains("K"), lit("error bloqueante")).otherwise(lit("error no bloqueante")))
      .withColumn("fecha_timestamp", current_timestamp())
      .filter(col("fails") =!= "")
      .groupBy("nombre_proceso", "entidad", "fecha_timestamp", "codigo_error", "descripcion_error", "tipo_error", "id_proceso").count
      .select(col("nombre_proceso"), col("entidad"), col("fecha_timestamp"), col("codigo_error"), col("descripcion_error"), col("tipo_error"), col("id_proceso"), col("count").as("numero_errores"))
    valD.unionByName(a)
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def cod_error(string: Column)(implicit sparkSession: SparkSession): Column = {
    when(string.contains("warn"), when(string.contains("null"), lit("W1"))
      .otherwise(lit("W2")))
      .otherwise(when(string.contains("null"), lit("K1"))
        .otherwise("K2"))
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def desc_error(cod: Column)(implicit sparkSession: SparkSession): Column = {
    when(cod === "W1", "No existe cruce con la columna nullable ")
      .when(cod === "W2", "No recupera nada del cruce con la columna nullable ")
      .when(cod === "K1", "No existe cruce con la columna obligatoria ")
      .otherwise("No recupera nada del cruce con la columna obligatoria ")
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def valDetalleFunc(validacion_df: DataFrame, entidad: String, nombreProc:String,colEstado:String,colAux:String, valD: DataFrame)(implicit sparkSession: SparkSession): DataFrame = {
    val a = validacion_df.filter(col(colEstado) =!= "ok").select(explode(split(col(colAux), "\t")).as("fails"))
      .withColumn("nombre_proceso", lit(nombreProc))
      .withColumn("entidad", lit(entidad))
      .withColumn("codigo_error", split(col("fails"),"-")(1))
      .withColumn("id_proceso", concat(col("nombre_proceso"),lit(" "),col("entidad")))
      .withColumn("descripcion_error", split(col("fails"), "-")(2))
      .withColumn("tipo_error", when(col("codigo_error").contains("ko"), lit("error bloqueante")).otherwise(lit("error no bloqueante")))
      .withColumn("fecha_timestamp", current_timestamp())
      .filter(col("fails") =!= "ok" && col("fails") =!= "")
      .groupBy("nombre_proceso", "entidad", "fecha_timestamp", "codigo_error", "descripcion_error", "tipo_error", "id_proceso").count
      .select(col("nombre_proceso"), col("entidad"), col("fecha_timestamp"), col("codigo_error"), col("descripcion_error"), col("tipo_error"), col("id_proceso"), col("count").as("numero_errores"))
    a.unionByName(valD)
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% FIN FUNCIONES AUXILIARES %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
}



