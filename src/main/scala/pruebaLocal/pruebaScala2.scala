package pruebaLocal

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

object pruebaScala2 {
  val logg = Logger.getLogger("org")
  logg.setLevel(Level.WARN)

  def main(args: Array[String]) {

    implicit val spark = SparkSession
      .builder().master("local[1]")
      .appName("Prueba_Local")
      .getOrCreate()

    import spark.implicits._

    val input = "src/main/test2/input/electric-chargepoints-2017.csv"
    val output = "src/main/test2/output/salida.csv"

    val data = extract(input)
    data.show(5,false)

    val dataT = transform_m(data)
    dataT.show(5,false)

    //dataT.where(col("CPID")==="AN06056").show(false)
    saveas(dataT,output)
  }

  def extract(input:String)(implicit spark:SparkSession):DataFrame ={
    val data = spark.read.option("header", "true").csv(input)
      .withColumn("Start",concat(col("StartDate"),lit(" "),col("StartTime")).cast("timestamp"))
      .withColumn("End",concat(col("EndDate"),lit(" "),col("EndTime")).cast("timestamp"))
    data.printSchema()
    data
  }
  def transform_m(df:DataFrame)(implicit spark:SparkSession):DataFrame= {
    val df1 = df.select("CPID","Start","End").withColumn("dif",unix_timestamp(col("End"))-unix_timestamp(col("Start")))
    df1.printSchema()
    df1.groupBy("CPID").agg(round((max(col("dif"))/3600),2).alias("max_duration"),round((mean(col("dif"))/3600),2).alias("avg_duration"))
      .withColumnRenamed("Media","avg_duration")
  }

  def saveas(df:DataFrame,ruta:String)(implicit spark:SparkSession):Unit = {
    df.write.parquet(ruta)
  }

}
