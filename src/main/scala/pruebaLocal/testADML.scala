package pruebaLocal

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.catalyst.expressions.aggregate.Max
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Column, DataFrame, SparkSession}

object testADML {
  val logg = Logger.getLogger("org")
  logg.setLevel(Level.WARN)

  def main(args: Array[String]): Unit = {
    implicit val spark = SparkSession
      .builder().master("local[1]")
      .appName("Prueba_Local")
      .getOrCreate()

    import spark.implicits._

    val a = 108
    val b = 14
    val c = (a*b).toBinaryString
    println(c)
    println((a*b).toBinaryString.split("").toList.filter( _ =="1").length)


    //val b = Array(-5,3,-2,-1,5,6).toList.sorted.
    //for (i <- 1 to a.max +1) {if a.contains(i)}
    //val respuesta = if (maximo < 0){1}
    //else{a.sorted}
    //println(respuesta)

    //val input = "data/input/...csv"
    //val output = "adta/output/..."

    //spark.read.option("header", "true").csv(input)

  } //main end

  //def funcAux(input:String)(implicit spark:SparkSession):DataFrame ={}

} //object end
