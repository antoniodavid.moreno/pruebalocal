package pruebaLocal

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.functions._
import org.apache.spark.sql.{Column, DataFrame, SparkSession}


object pruebaScala1 {
  val logg = Logger.getLogger("org")
  logg.setLevel(Level.WARN)

  def main(args: Array[String]) {

    implicit val spark = SparkSession
      .builder().master("local[1]")
      .appName("Prueba_Local")
      .getOrCreate()

    import spark.implicits._

    val rute1 = "src/main/csv_files/data/england_councils/"
    val rute2 = "src/main/csv_files/data/"
    val df1 = extract_councils(rute1)
    df1.show(5,false)
    print(df1.count())

    val df2 = extract_avg_price(rute2,df1)
    df2.show(10,false)
  } // Fin main

  def extract_councils(rute1:String)(implicit spark:SparkSession): DataFrame = {
    val df1 = spark.read.option("delimiter", ",").option("header", "true").csv(rute1 + "district_councils.csv").withColumn("councilType1",lit("District Council"))
    val df2 = spark.read.option("delimiter", ",").option("header", "true").csv(rute1 + "district_councils.csv").withColumn("councilType2",lit("Borough"))
    val df3 = spark.read.option("delimiter", ",").option("header", "true").csv(rute1 + "district_councils.csv").withColumn("councilType3",lit("Metropolitan District"))
    val df4 = spark.read.option("delimiter", ",").option("header", "true").csv(rute1 + "district_councils.csv").withColumn("councilType4",lit("Unitary authority"))
    df1.union(df2).union(df3).union(df4).dropDuplicates("council", "county")
    //df1.join(df2,Seq("council","county"),"full").join(df3,Seq("council","county"),"full").join(df4,Seq("council","county"),"full")
  }
def extract_avg_price(rute2:String,df1:DataFrame)(implicit spark:SparkSession): DataFrame ={
  val df = spark.read.option("delimiter", ",").option("header", "true").csv(rute2 + "property_avg_price.csv").select("local_authority","avg_price_nov_2019")
  df
}
}
