package pruebaLocal

import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._
import org.apache.spark.sql.{DataFrame, SparkSession}

/**
 * Unai Iparraguirre
 * David Moreno
 */

object App {
  
  def main(args : Array[String]) {

   implicit val spark = SparkSession
      .builder().master("local[1]")
      .appName("pruebaLocal")
      .getOrCreate()

    import spark.implicits._
/**
    /**  Importación muestra de datos provenientes de f0 **/
    val muestra = spark.read.option("delimiter", ",").option("header", "true").csv("src/main/Datos/Muestra.csv")
    muestra.show(5,false)

    /**  Prueba def aux **/
    //prueba1(muestra)
    /**  Prueba suma **/
    //println("El valor es")
    //println(suma(1,2))
    /**  Prueba concate **/
    //val df2 = muestra.withColumn("concatena",concate(col("cod_ramo"),col("cod_modalidad")))
    //df2.select("cod_ramo","cod_modalidad","concatena").show(5,false)

    /**  Importacion modelo_f1 con su vigencia **/
    val modelo_f1 = spark.read.option("delimiter", ",").option("header", "true").csv("src/main/Datos/modelo_f1_vig.csv")
    modelo_f1.show(false)

*/
    val lista1 = List(1,2)

    println(List(5,6)::List(3,4) :: lista1)

  } //FIN MAIN
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def prueba1(df:DataFrame)(implicit spark:SparkSession):Unit={
    df.show(5,false)
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  def suma(a:Float,b:Float)(implicit spark:SparkSession):Float={
    val c = a+b
    return c
  }
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%



  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  // %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
} // FIN APP