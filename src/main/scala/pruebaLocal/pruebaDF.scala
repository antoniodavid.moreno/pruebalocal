package LucaLocal

import org.apache.log4j.{Level, Logger}
import org.apache.spark.sql.SparkSession
import org.apache.spark.sql.functions._
import org.apache.spark.sql.types._

object pruebaDF {
  val logg = Logger.getLogger("org")
  logg.setLevel(Level.WARN)

  def main(args : Array[String]) {

    implicit val spark = SparkSession
      .builder().master("local[1]")
      .appName("pruebaLocal")
      .getOrCreate()

    import spark.implicits._

    /** Importacion modelo_f1 con su vigencia * */
    val modelo_f1 = spark.read.option("delimiter", ";").option("header", "true").csv("src/main/Datos/tablas_param/param_tr_mult.csv")

    val a = modelo_f1.select("cod_sociedad","sist_origen","cod_ramo_cont","cod_modalidad_cont").dropDuplicates()

    a.coalesce(1).write.option("delimiter",";").option("header", "true").csv("src/main/Datos/tablas_param/param_tr_mult_nueva.csv")

  }
}
